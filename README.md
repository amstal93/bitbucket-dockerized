### I. Prerequisites
`docker` 20.10+, `docker-compose` 1.29+

### II. Deployment guideline
##### 1. (Optional) Free up port 22
By default, SSH connection to `bitbucket` will be exposed on port `7999`, if you wanna enable SSH on port `22`, need to change the `sshd` port of the host into other port than `22`, e.g. `2222`:
```bash
sudo sed -i 's/#Port 22/Port 2222/' /etc/ssh/sshd_config
```

Restart the `sshd` service:
```bash
sudo systemctl restart ssh && sudo systemctl status ssh -n0 --no-pager
```

##### 2. Config and deploy
Update your `.env` like below:
```properties
COMPOSE_FILE=compose.yml:compose.acme.yml
DOMAIN_NAME=git.your-domain.com
ACCOUNT_EMAIL=you@your-domain.com
```
Your SSL cert will be renewed automatically every **60 days**, and configured with `OCSP Must Staple` and `Certificate Transparency`.

Moreover, your proxy will be enabled `TLS 1.3`, `OCSP Stapling`, `Forward Secrecy` and `HSTS`.

Start `nginx-proxy` and `nginx-proxy-acme` first:
```bash
docker-compose up -d nginx-proxy-acme && docker-compose logs -f nginx-proxy nginx-proxy-acme
```

Then start the other containers:
```bash
docker-compose up -d && docker-compose logs -f
```

##### 3. License your bitbucket
```bash
docker run --rm -v "${PWD}/atlassian-agent.jar:/atlassian-agent.jar" \
  openjdk:8-jre-alpine \
  java -jar atlassian-agent.jar \
  -m your@email.com \
  -n hino \
  -o akatsuki \
  -p bitbucket \
  -s ABCD-1234-EFGH-5678
```

##### 4. (Optional) Enable connections over SSH port 22
If you wanna use default port `7999` over port `22`, skip this step :penguin:

- First, add your public key: http://git.your-domain.com/plugins/servlet/ssh/account/keys/add

- Go to `Server Settings`: http://git.your-domain.com/admin/server-settings, change your `SSH base URL` into something like:
  ```
  ssh://git.your-domain.com
  ```
  Replace `git.your-domain.com` with your own domain name.
